package br.com.arlentadeu.neppoClient2.connection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConnectionFactory {
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");

	public EntityManager getConnection() {
		return emf.createEntityManager();
	}
	
	public void close() {
		emf.close();
	}

}
