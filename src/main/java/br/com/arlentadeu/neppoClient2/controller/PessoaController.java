package br.com.arlentadeu.neppoClient2.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import br.com.arlentadeu.neppoClient2.model.Pessoa;
import br.com.arlentadeu.neppoClient2.service.PessoaService;
import br.com.arlentadeu.neppoClient2.util.CustomErrorType;
 

@RestController
@RequestMapping(value = "/api", produces = "application/json")
public class PessoaController {

    public static final Logger logger = LoggerFactory.getLogger(PessoaController.class);

    @Autowired
    PessoaService pessoaService;
 
    public PessoaController(PessoaService pessoaService) {
		super();
		this.pessoaService = pessoaService;
	}

	@RequestMapping(
    		value = "/pessoa", 
    		produces = "application/json",
    		method = RequestMethod.GET)
    public ResponseEntity<List<Pessoa>> listAllPessoas() {
        List<Pessoa> pessoas = pessoaService.findAllPessoas();
        if (pessoas.isEmpty()) return new ResponseEntity(HttpStatus.NO_CONTENT);
        return new ResponseEntity<List<Pessoa>>(pessoas, HttpStatus.OK);
    }

    @RequestMapping(
    		value = "/pessoa/{id}", 
    		produces = "application/json",
    		method = RequestMethod.GET)
    public ResponseEntity<?> getPessoa(@PathVariable("id") long id) {
        logger.info("Recuperando Pessoa de ID " + id);
        Pessoa pessoa = pessoaService.findById(id);
        if (pessoa == null) {
            logger.error("Pessoa de id " + id + " não encontrada");
            return new ResponseEntity(new CustomErrorType("Pessoa de id " + id + " não encontrada"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Pessoa>(pessoa, HttpStatus.OK);
    }

    @RequestMapping(
    		value = "/pessoa", 
    		method = RequestMethod.POST)
    public ResponseEntity<?> createPessoa(@RequestBody Pessoa pessoa) {
        logger.info("Cadastrando uma nova Pessoa  : " + pessoa);

        if (pessoaService.findByDocumento(pessoa.getDocumento()) != null) {
            logger.error("Não foi possível cadastrar esta pessoa. Já existe uma pessoa com o documento: " + pessoa.getDocumento());
            return new ResponseEntity(new CustomErrorType("Não foi possível cadastrar esta pessoa. Já existe uma pessoa com o documento: " + pessoa.getDocumento()),HttpStatus.CONFLICT);
        }

        String errorMsg = checkConstraints(pessoa);
        if(errorMsg != null) {
            logger.error("Não foi possível cadastrar esta pessoa. " + errorMsg);
            return new ResponseEntity(new CustomErrorType("Não foi possível cadastrar esta pessoa. " + errorMsg),HttpStatus.CONFLICT);
        }
        pessoaService.savePessoa(pessoa);

        return new ResponseEntity<Pessoa>(pessoa, HttpStatus.OK);
    }

    private String checkConstraints(Pessoa pessoa) {
        if (pessoa.getNome() == null) {
            return "Campo [Nome] não pode ser nulo!";
        }

        if (pessoa.getDocumento() == null) {
            return "Campo [documento] não pode ser nulo!";
        }

        if (pessoa.getDataNascimento() == null) {
            return "Campo [Data de Nascimento] não pode ser nulo!";
        }

        if (pessoa.getSexo() == null) {
            return "Campo [Sexo] não pode ser nulo!";
        }

    	return null;
    }
    
    @RequestMapping(
    		value = "/pessoa", 
    		method = RequestMethod.PUT)
    public ResponseEntity<?> updatePessoa(@RequestBody Pessoa pessoa) {
        if (pessoa == null) {
            logger.error("RequestBody vazio!");
            return new ResponseEntity(new CustomErrorType("RequestBody vazio!"), HttpStatus.BAD_REQUEST);
        }

    	Long id = pessoa.getId();
        logger.info("Atualizando a Pessoa de ID: " + id);

        Pessoa currentPessoa = pessoaService.findById(id);

        if (currentPessoa == null) {
            logger.error("Não foi possível atualizar esta pessoa. Pessoa com o ID " + id + " não localizada");
            return new ResponseEntity(new CustomErrorType("Não foi possível atualizar esta pessoa. Pessoa com o ID " + id + " não localizada"), HttpStatus.NOT_FOUND);
        }

        String errorMsg = checkConstraints(pessoa);
        if(errorMsg != null) {
            logger.error("Não foi possível atualizar esta pessoa. " + errorMsg);
            return new ResponseEntity(new CustomErrorType("Não foi possível atualizar esta pessoa. " + errorMsg),HttpStatus.CONFLICT);
        }

        pessoa.setId(currentPessoa.getId());
        pessoaService.updatePessoa(pessoa);
        return new ResponseEntity<Pessoa>(pessoa, HttpStatus.OK);
    }

    @RequestMapping(
    		value = "/pessoa/{id}", 
    		method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePessoa(@PathVariable("id") long id) {
        logger.info("Deletando a Pessoa de ID: " + id);

        Pessoa pessoa = pessoaService.findById(id);
        if (pessoa == null) {
            logger.error("Não foi possível deletar esta pessoa. Pessoa com o ID " + id + " não localizada");
            return new ResponseEntity(new CustomErrorType("Não foi possível deletar esta pessoa. Pessoa com o ID " + id + " não localizada"), HttpStatus.NOT_FOUND);
        }
        pessoaService.deletePessoaById(id);
        return new ResponseEntity<Pessoa>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(
    		value = "/pessoa/", 
    		method = RequestMethod.DELETE)
    public ResponseEntity<Pessoa> deleteAllPessoas() {
        logger.info("Deletando todas as Pessoas");

        pessoaService.deleteAllPessoas();
        return new ResponseEntity<Pessoa>(HttpStatus.NO_CONTENT);
    }

    /*
    @RequestMapping(
    		value = "/list", 
    		method = RequestMethod.GET,
    		produces = "application/json")
    public List<Pessoa> list() {
        return new PessoaDAO().list();
    }

    @RequestMapping(
    		value = "/get/{documento}", 
    		method = RequestMethod.GET,
    		produces = "application/json")
    public List<Pessoa> get(@PathVariable("documento") String documento) {
    	PessoaDAO dao = new PessoaDAO();
        return dao.find(documento);
    }

    @RequestMapping(
    		value = "/post", 
    		method = RequestMethod.POST,
    		consumes = "application/json",
    		produces = "application/json")
    public Pessoa post(@RequestBody Pessoa pessoa) {
    	PessoaDAO dao = new PessoaDAO();
        return dao.save(pessoa);
    }
*/
    
}
