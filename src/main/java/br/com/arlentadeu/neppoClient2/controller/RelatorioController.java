package br.com.arlentadeu.neppoClient2.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import br.com.arlentadeu.neppoClient2.model.Relatorio;
import br.com.arlentadeu.neppoClient2.service.RelatorioService;
 

@RestController
@RequestMapping(value = "/api", produces = "application/json")
public class RelatorioController {

    public static final Logger logger = LoggerFactory.getLogger(RelatorioController.class);

    @Autowired
    RelatorioService relatorioService;
 
    public RelatorioController(RelatorioService relatorioService) {
		super();
		this.relatorioService = relatorioService;
	}

	@RequestMapping(
    		value = {"/relatorio", "/relatorio/", "/relatorio/{id}"}, 
    		produces = "application/json",
    		method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
    public ResponseEntity<List<Relatorio>> relatorio() {
		List<Relatorio> rel = relatorioService.geraRelatorio();
		logger.info("Relatorio gerado : \n" + rel.toString());
        return new ResponseEntity<List<Relatorio>>(rel, HttpStatus.OK);
    }

    
}
