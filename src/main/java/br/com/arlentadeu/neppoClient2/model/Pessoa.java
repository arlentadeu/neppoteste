package br.com.arlentadeu.neppoClient2.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "pessoa")
@EntityListeners(AuditingEntityListener.class)
public class Pessoa implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
	@NotNull
    @Column(name = "nome")
	private String nome;
	
	@NotNull
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "dataNascimento")
	private Date dataNascimento;
	
	@NotNull
    @Column(name = "documento")
	private String documento;
	
	@NotNull
    @Column(name = "sexo")
	private String sexo;
	
    @Column(name = "endereco")
	private String endereco;
	
	public Pessoa() {}

	
	public Long getId() {
		return id;
	}
 

	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", dataNascimento=" + dataNascimento + ", documento=" + documento + ", sexo="
				+ sexo + ", endereco=" + endereco + "]";
	}
	
	
}
