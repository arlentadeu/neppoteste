package br.com.arlentadeu.neppoClient2.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "relatorio")
@EntityListeners(AuditingEntityListener.class)
public class Relatorio implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "homens")
    private Long homens = 0L;

    @Column(name = "mulheres")
    private Long mulheres = 0L;

    @Column(name = "faixa1")
    private Long faixa1 = 0L;

    @Column(name = "faixa2")
    private Long faixa2 = 0L;

    @Column(name = "faixa3")
    private Long faixa3 = 0L;

    @Column(name = "faixa4")
    private Long faixa4 = 0L;

    @Column(name = "faixa5")
    private Long faixa5 = 0L;
    
	public Relatorio() {}

	public Long getHomens() {
		return homens;
	}

	public void setHomens(Long homens) {
		this.homens = homens;
	}

	public Long getMulheres() {
		return mulheres;
	}

	public void setMulheres(Long mulheres) {
		this.mulheres = mulheres;
	}

	public Long getFaixa1() {
		return faixa1;
	}

	public void setFaixa1(Long faixa1) {
		this.faixa1 = faixa1;
	}

	public Long getFaixa2() {
		return faixa2;
	}

	public void setFaixa2(Long faixa2) {
		this.faixa2 = faixa2;
	}

	public Long getFaixa3() {
		return faixa3;
	}

	public void setFaixa3(Long faixa3) {
		this.faixa3 = faixa3;
	}

	public Long getFaixa4() {
		return faixa4;
	}

	public void setFaixa4(Long faixa4) {
		this.faixa4 = faixa4;
	}

	public Long getFaixa5() {
		return faixa5;
	}

	public void setFaixa5(Long faixa5) {
		this.faixa5 = faixa5;
	}

	@Override
	public String toString() {
		return "Relatorio [homens=" + homens + ", mulheres=" + mulheres + ", faixa1=" + faixa1 + ", faixa2="
				+ faixa2 + ", faixa3=" + faixa3 + ", faixa4=" + faixa4 + ", faixa5=" + faixa5 + "]";
	}

	
}
