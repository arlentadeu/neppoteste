package br.com.arlentadeu.neppoClient2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.arlentadeu.neppoClient2.model.Pessoa;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
	Pessoa findByDocumento(String documento);
	
}