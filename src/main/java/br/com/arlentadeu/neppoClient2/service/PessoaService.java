package br.com.arlentadeu.neppoClient2.service;


import java.util.List;

import br.com.arlentadeu.neppoClient2.model.Pessoa;

public interface PessoaService {

    Pessoa findById(Long id);

    Pessoa findByDocumento(String documento);

    void savePessoa(Pessoa pessoa);

    void updatePessoa(Pessoa pessoa);

    void deletePessoaById(Long id);

    void deleteAllPessoas();

    List<Pessoa> findAllPessoas();
    

}
