package br.com.arlentadeu.neppoClient2.service;


import java.util.List;

import br.com.arlentadeu.neppoClient2.model.Relatorio;

public interface RelatorioService {

	Relatorio findById(Long id);

	List<Relatorio> geraRelatorio();
}
