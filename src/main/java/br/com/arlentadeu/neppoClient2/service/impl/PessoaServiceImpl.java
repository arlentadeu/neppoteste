package br.com.arlentadeu.neppoClient2.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arlentadeu.neppoClient2.repository.PessoaRepository;
import br.com.arlentadeu.neppoClient2.connection.ConnectionFactory;
import br.com.arlentadeu.neppoClient2.model.Pessoa;
import br.com.arlentadeu.neppoClient2.model.Relatorio;
import br.com.arlentadeu.neppoClient2.service.PessoaService;



@Service("pessoaService")
@Transactional
public class PessoaServiceImpl implements PessoaService{

    @Autowired
    private PessoaRepository pessoaRepository;

    public Pessoa findById(Long id) {
    	Optional<Pessoa> pessoa = null;
    	pessoa = pessoaRepository.findById(id);
    	if (pessoa.isPresent()) {
    		return pessoa.get();
    	}
        return null;
    }

    public Pessoa findByDocumento(String documento) {
    	return pessoaRepository.findByDocumento(documento);
    }
    
    public Pessoa getOne(Long id) {
        return pessoaRepository.getOne(id);
    }

    public void savePessoa(Pessoa pessoa) {
        pessoaRepository.save(pessoa);
    }

    public void updatePessoa(Pessoa pessoa){
        pessoaRepository.save(pessoa);
    }

    public void deletePessoa(Pessoa pessoa){
        deletePessoa(pessoa);
    }

    public void deletePessoaById(Long id){
        pessoaRepository.deleteById(id);
    }

    public void deleteAllPessoas(){
        pessoaRepository.deleteAll();
    }

    public List<Pessoa> findAllPessoas(){
        return pessoaRepository.findAll();
    }

}
