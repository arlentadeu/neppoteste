package br.com.arlentadeu.neppoClient2.service.impl;

import java.util.List;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arlentadeu.neppoClient2.repository.RelatorioRepository;
import br.com.arlentadeu.neppoClient2.model.Relatorio;
import br.com.arlentadeu.neppoClient2.service.RelatorioService;



@Service("relatorioService")
@Transactional
public class RelatorioServiceImpl implements RelatorioService{

	@Autowired
	private RelatorioRepository relatorioRepository;


	public List<Relatorio> geraRelatorio() {
		Configuration config = new Configuration().configure().addAnnotatedClass(Relatorio.class);
		ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();

		SessionFactory sf = config.buildSessionFactory(registry);
		Session session = sf.openSession();

		session.beginTransaction();
		final String sql = "SELECT \n" + 
				"	1 as id,\n" + 
				"	SUM(D.M) AS homens,  \n" + 
				"	SUM(D.F) AS mulheres,\n" + 
				"	SUM(D.FAIXA1) AS faixa1,  \n" + 
				"	SUM(D.FAIXA2) AS faixa2,  \n" + 
				"	SUM(D.FAIXA3) AS faixa3,  \n" + 
				"	SUM(D.FAIXA4) AS faixa4,  \n" + 
				"	SUM(D.FAIXA5) AS faixa5\n" + 
				"FROM (\n" + 
				" select \n" + 
				" 	IF(YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(c.data_nascimento))) < 10, 1, 0) AS faixa1, \n" + 
				" 	IF( YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(c.data_nascimento))) between 10 and 19, 1, 0) AS faixa2, \n" + 
				" 	IF( YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(c.data_nascimento))) between 20 and 29, 1, 0) AS faixa3, \n" + 
				" 	IF( YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(c.data_nascimento))) between 30 and 39, 1, 0) AS faixa4, \n" + 
				" 	IF( YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(c.data_nascimento))) >= 40, 1, 0 ) AS faixa5, \n" + 
				"	 \n" + 
				"	 IF(c.sexo='M', 1, 0) AS M,\n" + 
				"	 IF(c.sexo='F', 1, 0) AS F\n" + 
				"	 \n" + 
				"	  from pessoa c\n" + 
				"	  ) D";
		Query q = session.createNativeQuery(sql, Relatorio.class);
		List<Relatorio> rels = q.getResultList();

		return rels;
	}


	public Relatorio findById(Long id) {
		return null;
	}
}
