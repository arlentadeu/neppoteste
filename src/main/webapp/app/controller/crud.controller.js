angular.module("crudApp").controller("CrudController", CrudController);

CrudController.inject = [ '$scope', 'Pessoa' ];

function CrudController($scope, Pessoa) {
	
	$scope.pessoas = Pessoa.query();

	$scope.pessoa = {};
	
	$scope.buttonText="Atualizar";
	
	$scope.savePessoa = function() {
		if ($scope.pessoa.id !== undefined) {
			Pessoa.update($scope.pessoa, function() {
				$scope.pessoas = Pessoa.query();
				$scope.pessoa = {};
				$scope.buttonText="Salvar";
			});
		} else {
			Pessoa.save($scope.pessoa, function() {
				$scope.pessoas = Pessoa.query();
				$scope.pessoa = {};
			});
		}
	}

	$scope.cancel = function(pessoa) {
		$scope.pessoa = {};
		$scope.pessoas = Pessoa.query();
	}

	$scope.updatePessoaInit = function(pessoa) {
		$scope.pessoa = pessoa;
	}

	$scope.deletePessoa = function(pessoa) {
		pessoa.$delete({id: pessoa.id}, function() {
			$scope.pessoas = Pessoa.query();
		});
	}
	
}

