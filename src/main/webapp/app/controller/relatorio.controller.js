angular.module("crudApp").controller("RelatorioController", RelatorioController);

RelatorioController.inject = [ '$scope', 'Relatorio' ];

function RelatorioController($scope, Relatorio) {
	  var resp = Relatorio.query();
	  resp.$promise.then(function() {
		  var rel = resp[0];
		  console.log(rel);
		  console.log(rel.homens);
		  
			$scope.generoLabels = ["Homens", "Mulheres"];
			$scope.generoSeries = ["Num. Pessoas"];
			$scope.generoData = [rel.homens, rel.mulheres];
		   $scope.generoColors = ["rgba(0,0,128,0.6)","rgba(255,99,132,0.6)"];
		   $scope.generoOptions = {
		           scales: {
		               yAxes: [{
		                   ticks: {
		                       beginAtZero:true
		                   }
		               }]
		           }

		   };

			$scope.faixaEtariaLabels = ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"];
			$scope.faixaEtariaSeries = ["Num. Pessoas"];
			$scope.faixaEtariaData = [rel.faixa1, rel.faixa2, rel.faixa3, rel.faixa4, rel.faixa5];
		   $scope.faixaEtariaColors = [
		   	'rgb(54,162,235,0.6)',
		   	'rgb(255,206,86,0.6)',
		   	'rgb(75,192,192,0.6)',
		   	'rgb(153,102,255,0.6)',
		   	'rgb(34,139,34,0.6)'
		];
		   $scope.faixaEtariaOptions = {
		           scales: {
		               yAxes: [{
		                   ticks: {
		                       beginAtZero:true
		                   }
		               }]
		           }

		   };

		  
		});
	  
}

