var crudApp = angular.module("crudApp", ['ngRoute', 'ngResource', 'chart.js']);

crudApp.config(function ($routeProvider) {
	
	$routeProvider
	
	.when("/", {
		templateUrl: "template/crud.html",
		controller: "CrudController"
	})
	
	.when("/relatorio", {
		templateUrl: "template/relatorio.html",
		controller: "RelatorioController"
	})
	
	.otherwise({ redirectTo: '/' });
})

;