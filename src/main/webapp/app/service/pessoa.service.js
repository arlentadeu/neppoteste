angular.module('crudApp').factory('Pessoa', Pessoa);

Pessoa.$inject = [ '$resource' ];

function Pessoa($resource){
	let resourceUrl = 'api/pessoa/:id';
	
	return $resource(resourceUrl, {}, {
		'update' : {
			method : 'PUT'
		}
	});
}