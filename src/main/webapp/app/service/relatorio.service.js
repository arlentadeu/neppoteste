angular.module('crudApp').factory('Relatorio', Relatorio);

Relatorio.$inject = [ '$resource' ];

function Relatorio($resource){
	let resourceUrl = 'api/relatorio/:id';
	
	return $resource(resourceUrl);
}